Guix Config
===========

An attempt to turn Eauchat into a reliable infrastructure, while remaining on a shoestring and maintaining a clear view of what's going on.

For now, everything happens in the [wiki](https://framagit.org/eauchat/guix-config/-/wikis/home).
